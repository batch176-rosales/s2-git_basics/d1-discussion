export const icons = [
    {
        id: 1,
        title: "Web広告の",
        textLabel1:"プランをつくる",
        textLabel2:"最短 7月1日（金） から掲載可能",
        middleText1:"広告バナーは",
        link:"広告バナー 制作ツール",
        middleText2:"で制作いただけます",
        image: "/assets/images/icon/web.png"
    },
    {
        id: 2,
        title: "YouTube広告の",
        textLabel1:"プランをつくる",
        textLabel2:"最短 7月1日（金） から掲載可能",
        middleText1:"広告動画は",
        link:" 広告動画 制作ツール",
        middleText2:"で制作いただけます",
        image: "/assets/images/icon/youtube.png"
        
    },
    {
        id: 3,
        title: "折込チラシの",
        textLabel1:"プランをつくる",
        textLabel2:"",
        middleText1:"",
        link:"",
        middleText2:"",
        image: "/assets/images/icon/flyer.png"
       
    },
    {
        id: 4,
        title: "ポスティングの",
        textLabel1:"ランをつくる",
        textLabel2:"",
        middleText1:"",
        link:"",
        middleText2:"",
        image: "/assets/images/icon/posting.png"
    }
]