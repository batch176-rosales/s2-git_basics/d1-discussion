import React, { useState, useRef } from 'react'; //See important notes below
/* use case of useRef:
 1. you want to manipulate or access your DOM element (DOM Reference)
 2. useRef for storing (or to hold ) the previous state*/

const UseRefDemo1 = () => {
  const [name, setName] = useState('');
  const inputElement = useRef('');  //this is also applicable ==> useRef()
  const changeName = () => {
    setName('');
    inputElement.current.focus();
  };
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useRef Demo | Exercise 1: RESET NAME with focus **************`}
      </h3>
      <div>
        <input
          ref={inputElement}
          autoComplete="off"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <button onClick={changeName}>Reset</button>

        <div>My name is {name}</div>
      </div>
      <br />
    </>
  );
};

export default UseRefDemo1;
/* useRef has a type of object and has a property called current

*/
//View this link for more info about useRef: https://gitlab.com/training-projects4/hooksss/hooks/-/tree/master/src/components/understandingHooks/useRef
