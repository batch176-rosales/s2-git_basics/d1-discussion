import React, { Component } from 'react';

export default class Ccomponent extends Component {
  state = new Date().toString();

  render() {
    return (
      <>
        <div>Class Component</div>
        <div>{this.state}</div>
      </>
    );
  }
}
