import React, { useState, useEffect } from 'react';

const UseEffectDemo3 = () => {
    const [windowWidth, setWindowWidth]= useState(window.innerWidth);
    console.log(windowWidth);

    const handleResize = () => {
        setWindowWidth(window.innerWidth)
    };

    useEffect(() => {
        window.addEventListener('resize', handleResize)

        return () => {
            window.removeEventListener('resize', handleResize)
        }
    
    }, [])
    
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useEffect Demo | Exercise 3: Dynamic Window Screen Resize **************`}
      </h3>
      <div>{windowWidth}</div>
    </>
  );
};

export default UseEffectDemo3;
