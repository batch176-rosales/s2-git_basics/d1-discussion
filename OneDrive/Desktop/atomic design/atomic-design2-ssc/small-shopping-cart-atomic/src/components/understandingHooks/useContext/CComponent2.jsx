import React, { Component } from 'react'
import { ThemeContext } from './UseContextDemo2'

export default class CComponent2 extends Component {
  themeStyles(toggle){
    return {
      backgroundColor: toggle ? '#333' : '#FFF',
      color: toggle ? '#fff' : '#333'
    }
  }
  render() {
    return (
      <ThemeContext.Consumer>
      <div>CComponent2</div>
      <div>{(toggle)=>{
        return <div style ={this.themeStyles(toggle)}>Class Theme</div>
      }}</div>
      </ThemeContext.Consumer>
    )
  }
}
