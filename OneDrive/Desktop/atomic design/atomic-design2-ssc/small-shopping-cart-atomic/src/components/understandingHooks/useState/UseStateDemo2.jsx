import React, { useState } from "react";

const UseStateDemo2 = () => {
  const [name, setName] = useState(false);
  const [cartLabel, setCartLabel] = useState(true);

  const changeName = () => {
    setName(!name);
  };

  return (
    <>
      <br /> <hr />
      <h3>
        *********** useState Demo | Exercise 2: Dynamic value | primitive data
        type: boolean **************
      </h3>
      <br />
      <div>
        {name ? "Hello, Debbie" : "Hello, Joy"}
        <div>
          <button onClick={changeName}>Click Me | Dynamic Value</button>
        </div>
      </div>
      <br />
      <div>
        <button onClick={() => setCartLabel(!cartLabel)}>
          {cartLabel ? "Add to Cart" : "Remove"}
        </button>
      </div>
    </>
  );
};

export default UseStateDemo2;
//Exercise2: When Button clicked it will display a changes of the value of the state;
// Hello Debbie >> Hello Joy (dynamic value | primitive data type: boolean)
