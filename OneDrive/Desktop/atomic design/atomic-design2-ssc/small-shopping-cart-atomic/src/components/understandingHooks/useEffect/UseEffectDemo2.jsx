import React, { useState, useEffect } from 'react';

const UseEffectDemo2 = () => {
  const [resourceType, setResourceType] = useState('Posts');
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/${resourceType}`)
      .then((response) => response.json())
      .then((json) => setItems(json));
  }, [resourceType]);

  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useEffect Demo | Exercise 2: Posts, User, Comments  **************`}
      </h3>
      <div>
        <button onClick={() => setResourceType('Posts')}>Posts</button>
        <button onClick={() => setResourceType('Users')}>Users</button>
        <button onClick={() => setResourceType('Comments')}>Comments</button>
        <div>{resourceType}</div>
        <div>
            //uncomment the codes in VS Code to see the API
          {/* {items.map((item) => {
            return <pre key={item.id}>{JSON.stringify(item)}</pre>;
          })} */}
        </div>
      </div>
    </>
  );
};

export default UseEffectDemo2;
