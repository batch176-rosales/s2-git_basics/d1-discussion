import React,{useContext} from 'react'
import { ThemeContext } from './UseContextDemo2'

export default function FComponent2() {
    const {toggle, setToggle} = useContext(ThemeContext);

    const themeStyle = {
        backgroundColor: toggle ? '#333' : '#FFF',
        color: toggle ? '#fff' : '#333'
      }
  return (
    <div style={themeStyle}>
    <div>Function Theme</div>
    </div>
  )
}
