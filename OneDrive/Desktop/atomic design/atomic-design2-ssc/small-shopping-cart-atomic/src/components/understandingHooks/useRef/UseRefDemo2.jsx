import React, { useState, useRef ,useEffect} from 'react';

/* use case of useRef:
 1. 
 2. useRef for STORING the previous state; and 
 3. HOLD mutable value prevent re-render of component*/

const UseRefDemo2 = () => {
  const [counter, setCounter] = useState(0);
  const prevRef = useRef(0);
  /* note: we use the useEffect here coz when we want to call this useEffect whenever the random counter changes */
  useEffect(()=>{
    prevRef.current = counter;

  },[counter]);

  const randomNum = () => {
    setCounter(Math.ceil(Math.random() * 500));
  };
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useRef Demo | Exercise 2: GENERATE RANDOM NUMBERS storing the  previous state**************`}
      </h3>
      <div>Random Counter : {counter}</div>

      <div>Previous Counter: {prevRef.current}</div>
      <div>
        <button onClick={randomNum}>Generate number</button>
      </div>
      <br />
    </>
  );
};

export default UseRefDemo2;
