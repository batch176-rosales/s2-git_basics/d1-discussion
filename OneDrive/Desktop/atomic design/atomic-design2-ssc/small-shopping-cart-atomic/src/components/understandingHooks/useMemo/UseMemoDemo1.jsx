import React, { useState, useMemo } from 'react';
/* use cases:
1. optimize expensive operation
2. [you want to do a] Referential equality
//READ THE ADDITIONAL NOTE!!!!
 */
// useMemo>> its syntax is an arrow function (same with useEffect)
export default function UseMemoDemo1() {
  const [count, setCount] = useState(1);
  const [name, setName] = useState('');

  const result = useMemo(() => {
    return factorial(count);
  }, [count]);  //memoize the value of the function while in useCallback memoize the function

  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useMemo Demo | Exercise 1: Factorial **************`}
      </h3>
      <div>
        Factorial of {count} is: {result}
      </div>
      <div>
        <button onClick={() => setCount(count - 1)}>Decrement</button>
        <button onClick={() => setCount(count + 1)}>Increment</button>
      </div>
      <br />
      <div>Enter Name</div>
      <div>
        <input
          value={name}
          placeholder="enter name"
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      {/* <div>My name is {name}</div> */}
      <DisplayName name={name} />
    </>
  );
}

// use React.memo when you want to render the current state not affecting the other states [Referential equality of the props]
const DisplayName = React.memo(({name})=>{
  console.log('rendered');
  return <div>{`My name is ${name}`}</div>;
})


const factorial = (n) => {
  // assuming this function performs a heavy operation and creates a slowness or delay in your app
  // thus you can apply useMemo hook
  if (n < 0) {
    return -1;
  }
  if (n === 0) {
    return 1;
  }
  return n * factorial(n - 1);
};


/* Additional note: Just be careful that you should never use useMemo directly, first you write all
the code as it is and then you will start thinking that you need to make some of the functions to be 
optimized then you only use the useMemo bec when you use useMemo for every function or everything then 
its going to give you a performance overhead which is not good for your application */