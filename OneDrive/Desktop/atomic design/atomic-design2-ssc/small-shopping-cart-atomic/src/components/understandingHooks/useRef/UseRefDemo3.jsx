import React, { useState, useRef, useEffect } from 'react';

export default function UseRefDemo3() {
  const [name, setName] = useState('');
  const renderedTimes = useRef(0);

  useEffect(() => {
    renderedTimes.current = renderedTimes.current + 1;
  });

  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useRef Demo | Exercise 3: My Name with Rendered Count in the Input Fields**************`}
      </h3>
      <div>
        <input
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div>My name is {name}</div>
      <div>I rendered {renderedTimes.current} times </div>
      <br />
    </>
  );
}
