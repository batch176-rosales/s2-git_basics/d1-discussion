import React, { useState } from 'react';
import { useEffect } from 'react'; //See important notes below

const Fcomponent = () => {
  const [time, setTime] = useState(new Date().toString());
  const [message, setMessage] = useState(true);
  

    useEffect(() => {
        console.log('render');
        const interval = setInterval(showDate, 1000);
        console.log(interval);

        return () => {
            console.log('Cleanup of interval');
            clearInterval(interval)
        }


    }, [time]);

    const showDate = ()=> {
        setTime(new Date().toString())
    }
 
  return (
    <>
      <div>Function Component</div>
      <div>
        <div>{time}</div>
        <button onClick={showDate}>
          Show Date
        </button>
      </div>
      <div>
        <div>
          {message ? 'Functional Message' : 'Change Functional Message'}
        </div>
        <button onClick={() => setMessage(!message)}>Change Message</button>
      </div>
    </>
  );
};

export default Fcomponent; 

/*1. useEffect no arg = will result to be mounted to the component and side effects will get called every time in your element(e.g button | continuous rendering)
  2. useEffect with 2nd arg with empty array[] = component mounted ONLY(onMount); i.e. side effects will NOT trigger in all your elements(applicable in window screen resizing, also can be specify in your dependency BUT most cases this should be onMounted(empty array) )
  3.useEFfect with dependency = component mounted and side effects will only trigger to the dependency being specified.(applicable in showDate(setInterval), query API)
Additional note: the return function consider as your clean up | application: subscribe and unsubscribe to an API

  View this link for more info about useEffect:https://gitlab.com/training-projects4/hooksss/hooks/-/tree/master/src/components/understandingHooks/useEffect

 */
