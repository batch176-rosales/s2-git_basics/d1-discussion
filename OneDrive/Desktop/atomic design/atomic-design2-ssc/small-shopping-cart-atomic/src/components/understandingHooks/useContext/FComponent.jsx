import React,{useContext} from 'react';
import { CounterContext } from './CounterContext';


export default function FComponent() {
    // in Function Component, no need to use context Consumer, just do the syntax below:
    const {counter, setCounter} = useContext(CounterContext);

  return (
    <>
    {/* prop drilling */}
      <div>Function Component</div>
      {/* <div>{count}</div> */}
      <div>{counter}</div>
      <div>
        <button onClick={()=>setCounter(counter+1)}>Increment</button>
      </div>
      <div>***</div>
      <Fchild />
    </>
  );
}

const Fchild = () => {
    // const {value} = useContext(CounterContext);
    const {counter, setCounter} = useContext(CounterContext);

  return (
    <>
      <div>Function Child Component</div>
      {/* <div>{count}</div> */}
      {/* <div>{value}</div> */}
      <div>{counter}</div>
      <div>
        <button onClick={()=>setCounter(counter-1)}>Decrement</button>
      </div>
      <br/>
    </>
  );
};
