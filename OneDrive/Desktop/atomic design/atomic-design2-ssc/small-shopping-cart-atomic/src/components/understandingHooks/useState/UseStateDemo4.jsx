import React, { useState } from 'react';

const UseStateDemo4 = () => {
  const [name, setName] = useState('');
  const [addName, setAddName] = useState([]);
  console.log(addName);

  const addNames = (e) => {
    e.preventDefault();
    setAddName([...addName, { id: addName.length, name }]);
    setName('');
  };
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useState Demo | Exercise 4: List of Names | Non-primitive data type:Object(and Array)  **************`}
      </h3>
      <div>
        <form onSubmit={addNames}>
          <input
            type="text"
            value={name}
            placeholder="add names"
            onChange={(e) => setName(e.target.value)}
          />
          <button>Submit</button>
        </form>
        <div>
          <ul>
            {addName.map((name) => {
              return (
                <li key={name.id}>
                  {name.name}
                  <div>
                    <button>Delete</button>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
};

export default UseStateDemo4;

