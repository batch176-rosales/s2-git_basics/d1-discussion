import React, { useState, useRef } from 'react';
import { useEffect } from 'react';

// https://dmitripavlutin.com/react-useref-guide/

export default function UseRefDemo5() {
  const [count, setCount] = useState(0);
  const timerIdRef = useRef(0);

  const startHandler = () => {
    if (timerIdRef.current) {
      return;
    }
    timerIdRef.current = setInterval(() => setCount((c) => c + 1), 1000);
  };
  const stopHandler = () => {
    clearInterval(timerIdRef.current);
    timerIdRef.current = 0;
  };

  const resetHandler = () => {
    stopHandler();
    setCount(0);
  };

  useEffect(() => {
    return () => clearInterval(timerIdRef.current);
  }, []);

  return (
    <>
      <br />
      <hr />
      <h3>{`*********** useRef Demo | Exercise 5: STOPWATCH**************`}</h3>
      <div>Timer: {count}s</div>
      <div>
        <button onClick={startHandler}>Start</button>
        <button onClick={stopHandler}>Stop</button>
        <button onClick={resetHandler}>Reset</button>
      </div>
    </>
  );
}
