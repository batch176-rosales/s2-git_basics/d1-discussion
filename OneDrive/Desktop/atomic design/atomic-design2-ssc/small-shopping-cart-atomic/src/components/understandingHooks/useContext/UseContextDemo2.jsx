import React from 'react';
import { useState } from 'react';
import FComponent2 from './FComponent2';
import CComponent2 from './CComponent2';



export const ThemeContext = React.createContext();


export default function UseContextDemo2() {
  const [toggle, setToggle] = useState(false);

 
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useContext Demo | Exercise 2: TOGGLE THEME **************`}
      </h3>
      <div>
        <button onClick={() => setToggle(!toggle)}>Toggle Theme</button>
      </div>
      <ThemeContext.Provider value={{ toggle, setToggle }}>
      <FComponent2 />
      {/* <CComponent2 /> */}
      </ThemeContext.Provider>
      <div></div>
    </>
  );
}
