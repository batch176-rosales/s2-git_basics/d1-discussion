import React, { useState, useRef } from 'react';
import { useEffect } from 'react';

export default function UseRefDemo4() {
  const [name, setName] = useState('');
  const prevName = useRef('');

  useEffect(() => {
    prevName.current = name;
  }, [name]);

  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useRef Demo | Exercise 4: GENERATE your Name storing the  previous state**************`}
      </h3>
      <div>
        <input value={name} onChange={(e) => setName(e.target.value)} />
      </div>
      <div>
        My name is {name} and it is used to be {prevName.current}
      </div>
      <br />
    </>
  );
}
//Additional notes: You need to use useRef in order to persist (stays the same)     a value between renders if you're not using state
