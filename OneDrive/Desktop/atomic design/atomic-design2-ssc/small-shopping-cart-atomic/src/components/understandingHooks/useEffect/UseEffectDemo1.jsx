import React from 'react';
import { useState } from 'react';
import Ccomponent from './Ccomponent';
import Fcomponent from './Fcomponent';

const UseEffectDemo1 = () => {
  const [time, setTime] = useState(false);
  return (
    <>
      <br />
      <hr />
      <h3>
        {`***** useEffect Demo | Exercise 1: Toggle Button with Time & Date 
      (Understanding the Life Cycle Method such as: componentDidMount, componentDidUpdate, componentWillUnmount)  ****`}
      </h3>
      <div>
        <button onClick={()=>setTime(!time)}>Toggle Function Component</button>
        {/* <div>{time ? <Ccomponent/> : ""}</div> */}
        <div>{time ? <Fcomponent/> : ""}</div>
      </div>
      <br />
    </>
  );
};

export default UseEffectDemo1;
