import React from 'react';
import { useState } from 'react';
import FComponent from './FComponent';
import CComponent from './CComponent';
import { CounterContext } from './CounterContext';

export default function UseContextDemo() {
  const [counter, setCounter] = useState(0);
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useContext Demo | Exercise 1: Increment-Decrement with Functional Component Emphasis-No to Prop-Drilling **************`}
      </h3>
      <div>App Component</div>
      <div>{counter}</div>
      <div>
        <button onClick={()=>setCounter(counter+1)}>Increment</button>
        <button onClick={()=>setCounter(counter-1)}>Decrement</button>
      </div>
      <br/>
      <CounterContext.Provider value = {{ counter, setCounter }}>
      <FComponent />
      {/* <CComponent /> */}
      </CounterContext.Provider>
    </>
  );
}
