import React, { useState,useCallback } from 'react';
import List from './List';

/* use case:
1. Memoize the function (useCallback) vs Memoize the value (useMemo) of the function
2. Referential equality for functions */
/* useMemo doesnt return the function it only returns the value of the function while useCallback is returning to us the entire function */

export default function UseCallbackDemo() {
  const [num, setNum] = useState(1);
  const [toggle, setToggle] = useState(false);
  
  const getItems = useCallback(()=>{
    return [num, num +1, num +2]
  },[num]);

    const themeStyles = {
        backgroundColor: toggle ? '#333' : '#FFFF',
        color: toggle ? '#FFFF' : '#333'
    }
  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useCallBack Demo | Exercise 1: Toggle Theme with a Series of Numbers **************`}
      </h3>
      <div>
        <input
          type="number"
          value={num}
          onChange={(e) => setNum(parseInt(e.target.value))}
        />
      </div>
      <div style ={themeStyles}> 
        <div>
          <button onClick={() => setToggle(!toggle)}>Toggle Theme</button>
        </div>
        <List getItems={getItems}/> 
        {/* notice: the getItem is an function being passed a props */}
      </div>
      <br />
    </>
  );
}
