import React, { Component } from 'react';
import { CounterContext } from './CounterContext';

export default class CComponent extends Component {
//   state =
  render() {
    return (
      <>
        <div>Class Component</div>
        {/* <div>{this.props.count}</div> */}
        {/* declare here the context consumer (note: it's a form of an arrow function) */}
        <CounterContext.Consumer>
            {(value)=>{
                return <div>{value}</div>
            }}
        </CounterContext.Consumer>
        <br/>

      </>
    );
  }
}


