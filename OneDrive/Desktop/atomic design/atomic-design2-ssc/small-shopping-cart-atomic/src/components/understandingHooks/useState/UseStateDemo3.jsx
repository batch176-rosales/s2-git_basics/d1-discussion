import React from 'react';
import { useState } from 'react';

const UseStateDemo3 = () => {
  const [step, setStep] = useState(0);
  const [stepPrev, setStepPrev] = useState(0);
  const [quantity, setQuantity]= useState (1);
  const clamp = (min, max) => (value) => value <= min ? min : value >= max ? max : value;
  const clampValue = clamp(1, 10);
  const incrementQuantity = () => setQuantity(clampValue(quantity + 1));
  const decrementQuantity = () => setQuantity(clampValue(quantity - 1));

  const incrementSteps = () => {
    setStepPrev((prevState) => prevState + 1)
    setStepPrev((prevState) => prevState + 1)
  };

  const decrementSteps = () => {
    setStepPrev((prevState) => prevState - 1)
    setStepPrev((prevState) => prevState - 1)
  };
  
  return (
    <>
      <br />
      <hr />
      <h3>
        *********** useState Demo | Exercise 3: Counter | primitive data type:
        number **************
      </h3>
      <br />
      <div>Steps</div>
      <div>
        <button onClick={()=>setStep(step+1)}>+</button>
      {step}
        <button onClick={()=>setStep(step-1)}>-</button>
      </div>
      <br />
      <div>Steps with prevState</div>
      <div>
        <button onClick={incrementSteps}>+</button>
      {stepPrev}
        <button onClick={decrementSteps}>-</button>
      </div>
      <br />
      <div>Limited Stocks of 10</div>
      <div>
        <button onClick={decrementQuantity}>-</button>
        {quantity}
        <button onClick={incrementQuantity}>+</button>
      </div>
    </>
  );
};

export default UseStateDemo3;
