import React from 'react';
import { useState, useMemo, useEffect } from 'react';

export default function UseMemoDemo2() {
  const [num, setNum] = useState(0);
  const [theme, setTheme] = useState(true);

  const themeStyles = useMemo(()=>{
    return{
        backgroundColor: theme ? 'black' : 'white',
        color: theme ? 'white' : 'black',
      };
  },[theme]) 

  const double = useMemo(() => {
    return slowFunction(num);
  }, [num]);

  useEffect(() => {
    console.log('theme changed');
  }, [themeStyles]);

  return (
    <>
      <br />
      <hr />
      <h3>
        {`*********** useMemo Demo | Exercise 2: Double numbers with changing Color Theme **************`}
      </h3>
      <div>
        <input
          type="number"
          value={num}
          onChange={(e) => setNum(e.target.value)}
        />
      </div>
      <div>
        <button onClick={() => setTheme(!theme)}>Change Theme</button>
      </div>
      <div style={themeStyles}>{double}</div>
    </>
  );
}

const slowFunction = (n) => {
  console.log('calling slow function');
  for (let i = 0; i <= 1000000000; i++) {}
  return n * 2;
};
