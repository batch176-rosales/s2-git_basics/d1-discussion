import React, {useState} from 'react';



const UseStateDemo1 = ()=> {
    const [name, setName]=useState('');
   /*  note: Instead of putting the function inside the button,
    you can also create a function and call the its function name 
    and put it inside your button. See syntax below
    */

    /* 
    const changeName = () =>{
      setName('Joy gwapa');
    } */

  return (
    <>
     <br />
        <h3>*********** useState Demo  | Exercise 1: Static value | primitive data type: string **************</h3>
        <br />
        <div>
           <div>Hello, {name}</div> 
            <button onClick={()=>setName('Debbie')}>Click Me</button>
            {/* <button onClick={changeName}> Click Me</button> */}
        </div>
    </>
  )
};


export default UseStateDemo1;

//Exercise1: When Button clicked it will display a changes of the value of the state; 
// Hello Debbie >> Hello Joy (static value | primitive data type: string)

