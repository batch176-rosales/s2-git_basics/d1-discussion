import React from 'react';
// import AdPlanCreate from '../organisms/FS/AdPlanCreatee';
import CreateTopPageReport from '../organisms/myTask/CreateTopPageReport';
import UseStateDemo from '../organisms/UseStateDemo';
import Common from '../templates/Common';
import UseEffectTutorial from '../organisms/UseEffectTutorial';
import UseRefDemo from '../organisms/UseRefDemo';
import UseMemoDemo from '../organisms/UseMemoDemo';
import UseCallBackDemo from '../organisms/UseCallBackDemo';
import UseContextIndex from '../organisms/UseContextIndex';
import SampleUseContextReactOneLine from '../organisms/SampleUseContextReactOneLine';
import CounterTwoUseReducer from '../organisms/CounterTwoUseReducer';
import AddContactsReducer from '../organisms/AddContactsReducer';
import TodoUsingReducer from '../organisms/TodoUsingReducer';
import UseStateDemo1 from '../understandingHooks/useState/UseStateDemo1';
import UseStateDemo2 from '../understandingHooks/useState/UseStateDemo2';
import UseStateDemo3 from '../understandingHooks/useState/UseStateDemo3';
import UseStateDemo4 from '../understandingHooks/useState/UseStateDemo4';
import UseEffectDemo1 from '../understandingHooks/useEffect/UseEffectDemo1';
import UseEffectDemo2 from '../understandingHooks/useEffect/UseEffectDemo2';
import UseEffectDemo3 from '../understandingHooks/useEffect/UseEffectDemo3';
import UseRefDemo1 from '../understandingHooks/useRef/UseRefDemo1';
import UseRefDemo2 from '../understandingHooks/useRef/UseRefDemo2';
import UseRefDemo3 from '../understandingHooks/useRef/UseRefDemo3';
import UseRefDemo4 from '../understandingHooks/useRef/UseRefDemo4';
import UseRefDemo5 from '../understandingHooks/useRef/UseRefDemo5';
import UseMemoDemo1 from '../understandingHooks/useMemo/UseMemoDemo1';
import UseMemoDemo2 from '../understandingHooks/useMemo/UseMemoDemo2';
import UseCallbackDemo from '../understandingHooks/useCallback/UseCallbackDemo';
import UseContextDemo from '../understandingHooks/useContext/UseContextDemo';
import UseContextDemo2 from '../understandingHooks/useContext/UseContextDemo2';
import AdPlanCreate from '../templates/AdPlanCreate';
export default function Homepage() {
  return (
    <>
      {/* <Common />
  <UseStateDemo />
  <SampleUseContextReactOneLine/>
  <UseEffectTutorial/>
  <UseRefDemo />
  <UseMemoDemo />
  <UseCallBackDemo />
  <UseContextIndex />
  <CounterTwoUseReducer/>
  <AddContactsReducer /> */}
      {/* <TodoUsingReducer /> */}
      {/* the lines below are the summary exercises about all the hooks in Reactjs.
  Additional notes: All the source code of these exercises are found in "components folder>> understandingHooks*/}

      {/* 
  <UseStateDemo1 />
  <UseStateDemo2 />
  <UseStateDemo3 />
  <UseStateDemo4 />
  <UseEffectDemo1 />
  <UseEffectDemo2 />
  <UseEffectDemo3 />
  <UseRefDemo1 />
  <UseRefDemo2 />
  <UseRefDemo3 />
  <UseRefDemo4 />
  <UseRefDemo5 />
  <UseMemoDemo1 />
  <UseMemoDemo2 />
  <UseCallbackDemo/>
  <UseContextDemo/>
  <UseContextDemo2/> */}
      {/* <AdPlanCreate/> */}

      {/* <AdPlanCreate /> */}
      <CreateTopPageReport />
    </>
  );
}
