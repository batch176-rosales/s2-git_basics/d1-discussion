// import { Typography } from '@mui/material';
import { ProductMetaWrapper } from './styles';
import styled from 'styled-components';
import React,{Fragment } from 'react';
// import { withRouter } from 'react-router';
// import { H3 } from '@atoms/Typographies';
import H3 from './H3';
// import { Link } from '@mui/material';

export default function ProductMeta({ icon, matches }) {
  return (
    <ProductMetaWrapper>
      <TextWithStyled1>
      {icon.middleText1} &nbsp;
        <Link>
          {`${icon.link}\n`}
        </Link>
        </TextWithStyled1>
        <TextWithStyled2>{icon.middleText2}</TextWithStyled2>
    
     
    </ProductMetaWrapper>
  );
}
const TextWithStyled1 = styled.div`
  font-size: 10px;
  font-weight: 900;
  justify-content:center;
  align-items: center;
  white-space: pre-wrap;
  word-wrap: break-word;
  margin-top:10px;
//   line-height: 1.5rem;
//    margin-left: 40px;
`;
const TextWithStyled2 = styled.div`
  font-size: 10px;
  font-weight: 900;
  justify-content:center;
  align-items: center;
//   white-space: pre-wrap;
//   word-wrap: break-word;
margin-top:0;

`;
const Link = styled.a`
    color: #106ba3;
    text-decoration: none;
    text-decoration-color: #106ba3;
    font-weight: bold;
    cursor: pointer;

`;
