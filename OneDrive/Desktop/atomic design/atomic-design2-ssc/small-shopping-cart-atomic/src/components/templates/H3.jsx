import React from 'react';
import styled from 'styled-components';

const H3 = ({ children, ...props }) => <H3WithStyled {...props}>{ children }</H3WithStyled>;

const H3WithStyled = styled.h3`
    font-weight: 900;
`;

export default H3;