import {styled} from '@mui/system';
import { Button, IconButton } from '@mui/material';
import { Box } from '@mui/system';
// import { slideInBottom, slideInRight } from "../../animation";
import { Colors } from './theme';


export const PanelWrapper  = styled(Box)(({ theme }) => ({
  display: 'flex',
 justifyContent: 'space-between',
 alignItems: 'start',
 padding: '20px',
 width:'70%',
background:'white',
  borderRadius: '20px',
  boxShadow:'0px 0px 4px 2px rgba(0, 0, 0, 0.25)',
  // padding: '20px',
  cursor: 'pointer',
    '&:hover': {
        transform: 'scale(1.1)',
        transition: 'transform 300ms cubic-bezier(0.15, 0.85, 0.45, 1.75)',
        textDecoration: 'none',
    },
    [theme.breakpoints.up("md")]: {
      position: "relative",
    },
}));

export const PanelWrap = styled(Box)(({ theme }) => ({
  justifyContent: 'center',
  display: 'flex',
  alignItems: 'center',
}));

export const Panel = styled(Box)(({ theme }) => ({
  //  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  width:'70%',
  
  // check the width and h in the figma
  // width: '250px',
  // height: '100px',
  // background: Colors.light_gray,
  
}));

export const PanelImage = styled('img')(({ src, theme }) => ({
  src: `url(${src})`,
  width: '40%',
//   width: '100.34px',
// height: '80.09px',
  // width: '100px',
  // height: '70px',  
  // background: Colors.light_gray,
  // paddingRight: '10px',
    [theme.breakpoints.down("md")]: {
      width: "50%",
      padding: '10px',
    },
}));

export const ProductActionButton = styled(IconButton)(() => ({
  background: Colors.white,
  margin: 4,
}));
// export const IconWithText =styled ()=>({
  

// })

export const ProductFavButton = styled(Box)(({ theme }) => ({
  // margin: 4,
  // display: 'flex',
  // alignItems: 'center',
  // justifyContent:'center',
  fontWeight:'790',
  // wordWrap:'break-word',
  // whiteSpace:'pre-wrap',
  margin:5,
  top: 10,
  alignItems: 'center',
justifyContent:'center',
  
  [theme.breakpoints.up("md")]: {
    position: "relative",
   
    
  },
}));

export const ProductAddToCart = styled(Button, {
  shouldForwardProp: (prop) => prop !== "show",
})(({ show, theme }) => ({
  width: "120px",
  fontSize: "12px",
  [theme.breakpoints.up("md")]: {
    position: "absolute",    
    bottom: "2%",
    width: "300px",
    padding: "10px 5px",
    // animation:
    //   show &&
    //   `${slideInBottom} 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both`,
  },
  background: Colors.secondary,
  opacity: 0.9,
}));

export const ProductMetaWrapper = styled(Box)(({ theme }) => ({
  padding: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent:'center',
}));

export const PanelActionsWrapper  = styled(Box)(({ show, theme }) => ({ 
  paddingTop: '12px',
  // paddingLeft: '10px',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent:'end',
  fontSize:'18px',
  fontWeight:'800',
  fontFamily:'Rounded Mplus 1c',
  // paddingLeft: '30px',
  [theme.breakpoints.up("md")]: {
    // display: show ? 'visible' : 'none',
    position: "relative",
    // right:25,
    // top: '30%',
    // animation: show && `${slideInRight} 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both`,
  }
}));