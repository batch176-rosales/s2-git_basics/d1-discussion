import React from 'react';
import Header from '../organisms/Header';
import Main from '../organisms/Main';
import CartItems from '../organisms/CartItems';
import styled from 'styled-components';
import data from '../../data';

export default function Common() {
  const {products} = data;
  return (
    <>
    <Header />
    <CommonWithStyled>
      <Main products ={products} />
      <CartItems />
    </CommonWithStyled>
    </>
  )
};

const CommonWithStyled = styled.div`
display: flex;
justify-content: space-between;
`;
