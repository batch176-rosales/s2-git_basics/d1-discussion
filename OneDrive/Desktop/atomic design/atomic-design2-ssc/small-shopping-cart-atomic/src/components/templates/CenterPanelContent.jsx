import React from 'react';
import { Box, Button, Container, Grid } from '@mui/material';
// import { Grid } from "@mui/material";
import { icons } from '../../data/index';
import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';
import SingleIconDesktop from './SingleIconDesktop';

export default function CenterPanelContent() {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('md'));
    
    const renderIcons = icons.map((icon) => (
      <Grid
        item
        key={icon.id}
        sm={6}
        md={6}
        display="flex"
        flexDirection={'column'}
        alignItems="center"
      >
        <SingleIconDesktop icon={icon} matches={matches} />
      </Grid>
    ));
  return (
    <Container>
    <Grid
            container
            spacing={{ md: 3}}
            justifyContent="center"
            sx={{ margin: `20px 4px 10px 4px` }}
            columns={{ sm:12,md: 12}}
          >
            {renderIcons}
          </Grid>
          </Container>
  )
}
