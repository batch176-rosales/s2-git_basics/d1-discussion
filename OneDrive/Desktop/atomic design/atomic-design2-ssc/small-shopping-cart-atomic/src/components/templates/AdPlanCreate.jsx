import React from 'react'
import { Box, Grid } from '@mui/material';
import Screen from './Screen';
import {MyAdPlanContext} from './AdPlanContext';
import CenterPanel from './CenterPanel';

export default function AdPlanCreate() {
    // const [centerPanel]
    const classes = {
        wrapper: {
          backgroundColor: 'rgba(0, 0, 0, 0.65)',
          zIndex: '1',
          width: '100%',
          height: '100vh',
        },
        navbarCont: {
          height: '10%',
          width: '100%',
          padding: '1rem 1.5rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
        },
        screenCont: {
          height: '68%',
          width: '100%',
        },
        dialogueCont: {
          position: 'relative',
          zIndex: '5',
          height: '22%',
          width: '100%',
          paddingTop: '1rem',
        },
      };
  return (
    <Box sx={classes.wrapper}>
        <Box sx={classes.navbarCont}>
            
          </Box>

{/* Event screen */}
     <Box sx={classes.screenCont}>
            <Screen centerPanel={CenterPanel}
            //   toShow={true}
            //   centerPanelScreenType={getScreenType}
            //   centerPanel={CenterPanel}
            //   rightPanel={RightPanel}
            //   handleAutoCont={handleAutoCont}
            //   autoContinue={state.autoContinue}
            //   setState={setState}
            />
          </Box>
       {/* Dialogue Screen S */}
       <Box sx={classes.dialogueCont}>
          </Box>

  </Box>
  )
}
