import React from 'react';
import { Box, Button, Container, Grid } from '@mui/material';
// import { Grid } from "@mui/material";
import { icons } from '../../data/index';
import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';
import SingleIconDesktop from './SingleIconDesktop';

export default function CenterPanel() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('md'));
  
  const renderIcons = icons.map((icon) => (
    <Grid
      item
      key={icon.id}
      xs={6}
      sm={4}
      md={6}
      display="flex"
      flexDirection={'column'}
      alignItems="center"
    >
      <SingleIconDesktop icon={icon} matches={matches} />
    </Grid>
  ));
  const classes = {
    mainWrapper: {
      height: '96.5%',
      width: '100%',
      position: 'relative',
    },
    headContainer: {
      height: '10%',
      width: '100%',
      maxWidth: '100%',
      padding: '0 0 0.7rem 0.7rem',
    },
    headerTxt: {
      letterSpacing: '2px',
    },
    mainContainer: {
      height: '80%',
      width: '100%',
      maxWidth: '100%',
       padding: '1.5rem',
      //  margin:'1.5rem',
    },
    footerContainer: {
      height: '3%',
      width: '100%',
      maxWidth: '100%',
    },
    sectionMain: {
      maxWidth: '100%',
      width: '100%',
      height: '100%',
      maxHeight: '100%',
      padding:'1rem',
    },
    sectionFooter: {
      padding: '0.5rem 1rem',
      
    },
  };
  return (
    <Grid
      container
      item
      xs={12}
      justifyContent="space-between"
      //  justifyContent="center"
      alignItems="center"
      sx={classes.mainWrapper}
      direction="row"
    >
      {/* Header */}
      <Grid
        container
          // justifyContent="flex-start"
        alignItems="center"
        sx={classes.headContainer}
      >
        広告プランをつくる
        {/* <StyledTypography variant="h6" style={classes.headerTxt}>
        {newHeaderTxt}
      </StyledTypography> */}
      </Grid>
      {/* Header */}

      {/* Main */}
      <Grid
        item
        container
        justifyContent="center"
        alignItems="center"
        xs={12}
        sx={classes.mainContainer}
      >
        {/* <Grid
        container
        justifyContent="center"
        alignItems="center"
        item
        xs={12}
        sx={classes.sectionMain}
      > */}
        {/* <SubRoutes componentName={newComponentName} /> */}
        {/* subroutes */}
        {/* <Grid sx={classes.sectionMain}> */}
        {/* <Container> */}
          <Grid
            container
            spacing={{md: 3}}
            justifyContent={"center"}
            // sx={{ margin: `20px 4px 10px 4px`}}
            columns={{sm:8, md: 12}}
            sx={classes.sectionMain}
            
          >
            {renderIcons}
          {/* </Grid> */}
        </Grid>
        {/* </Container> */}
        {/* </Grid> */}
      </Grid>
      {/* Main */}

      {/* Footer  was deleted*/}
      <Grid container item xs={12} sx={classes.footerContainer}>
        {/* <MainFooter /> */}
      </Grid>
      {/* Footer */}
    </Grid>
  );
}

// https://smartdevpreneur.com/complete-guide-to-mui-box-shadow/
// paper
// https://github.com/grepsoft-zz/ecomm-mui/blob/master/src/components/products/SingleProductDesktop.js
// https://www.youtube.com/watch?v=MgKaxe9euI4

// recent one