import { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import {
  ExtraActionsWrapper,
  Panel,
  ProductActionButton,
  PanelActionsWrapper ,
  ProductAddToCart,
  ProductFavButton,
  //   ProductFavButton,
  PanelImage,
  ProductMetaWrapper,
  PanelWrapper ,
  PanelWrap,
} from './styles';
import { Stack, Tooltip, Typography } from '@mui/material';
// import FavoriteIcon from "@mui/icons-material/Favorite";
// import ShareIcon from "@mui/icons-material/Share";
// import FitScreenIcon from "@mui/icons-material/FitScreen";
// import useDialogModal from "../../hooks/useDialogModal";
// import ProductDetail from "../productdetail";
import ProductMeta from './ProductMeta';
import { Container } from '@mui/system';

export default function SingleIconDesktop({ icon, matches }) {
  return (
    <>
    <PanelWrap>
      <PanelWrapper>
        {/* <Product> */}
          <PanelImage src={icon.image} />

          <PanelActionsWrapper >
            <Stack direction="row">
            
              {icon.title}
              <br />
              {icon.textLabel1}
            </Stack>
          </PanelActionsWrapper>
        {/* </Product> */}
      </PanelWrapper>
      </PanelWrap>

      {/* <ProductMeta icon={icon} matches ={matches}/> */}

      {/* <ProductDetailDialog icon={icon} /> */}
    </>
  );
}
