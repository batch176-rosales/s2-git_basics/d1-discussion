import React, { useContext } from 'react'
import { Grid, Box } from '@mui/material';
import styled from 'styled-components';
import img from './interactive_bg.png';
import CenterPanel from './CenterPanel';
// import {MyAdPlanContext}


export default function Screen() {
    const classes = {
        container: {
          height: '100%',
          width: '100%',
        },
        h100: {
          height: '100%',
        },
        centerPanelCont: {
          height: '96.5%',
          width: '100%',
        //   backgroundImage: `url(${screenBg})`,
        backgroundImage: `url(${img})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: '100% 96.5%',
          position: 'relative',
        }, 
        explanationPartTxt: {
            padding: '2rem 0.2rem',
            color: '#FFF',
            borderRadius: '50%',
            fontWeight: 'bold',
            //background: newBorderColor,
            background: 'red',
            // border: `1px solid ${newBorderColor}`,
            border: `1px solid black`,
            textAlign: 'center',
          },
      };

    return(
    // const {getScreen}= useContext(AdPlanContext);
    // <Content>Screen</Content>
    <Grid container item xs={12} sx={classes.container}>
        <Grid item xs={3} />

        <Grid container item xs={6} sx={classes.centerPanelCont}>
        {/* {toShow && <CenterPanel />} */}
        {/* <Box sx={classes.explanationPartCont}>
            <StyledTypography variant="subtitle2" sx={classes.explanationPartTxt}>
            {lang.explanationTxt}
            </StyledTypography>
        </Box> */}
        {/* <Box sx ={classes.explanationPartTxt}>
            f
        </Box> */}
        <CenterPanel />

        </Grid>

        <Grid
        container
        item
        xs={3}
        sx={classes.h100}
        spacing={1}
        direction="column"
        alignItems="flex-end"
        justifyContent="flex-end"
        >
        {/* <RightPanel /> */}

        {/* <CustomizedAntSwitch
            language={language}
            setState={setState}
            leftLabel="Eng"
            rightLabel="Jap"
            color={newBorderColor}
        /> */}

        {/* <VisualNovelSwitch
            language={language}
            handleAutoCont={handleAutoCont}
            autoContinue={autoContinue}
        /> */}
        </Grid>
  </Grid>
  )
}


    
const Content = styled.div`
  border: 1px solid #000;
  background-image: url(${img});
  width: 2000px;
  height: 2000px;
`;