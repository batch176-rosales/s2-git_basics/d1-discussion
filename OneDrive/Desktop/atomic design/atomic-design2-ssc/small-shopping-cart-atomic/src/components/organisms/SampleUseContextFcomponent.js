import React,{useContext} from 'react';
import { CounterContext } from './SampleUseContextReactOneLine';


export default function SampleUseContextFcomponent() {
    const {count, setCount} = useContext(CounterContext);

  return (
    <>
    <div>Function Component</div>
    <div>{count}</div>
    <div><button onClick={()=>setCount(count+1)}>Increment</button>
    </div>
    <hr/>
    <ChildFunctionComponent/>
    </>
  )
};


const ChildFunctionComponent = () =>{
    const {count, setCount}=useContext(CounterContext);
    return(
        <>
        <div>Function Child Component</div>
        <div>{count}</div><div><button onClick={() => setCount(count - 1)}>Decrement</button>
        </div>
        </>
        
    )
}
