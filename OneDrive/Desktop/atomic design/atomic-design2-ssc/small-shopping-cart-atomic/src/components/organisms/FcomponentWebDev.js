import React,{ useContext } from 'react'
import { ThemeContext } from './UseContextIndex';

const FcomponentWebDev = ()=> {
    const {themeStyles} = useContext(ThemeContext);
  return (
    <>
    <div style={themeStyles}>Function Theme</div>
    <FunctionChild1Theme/>
    <br />
    <FunctionChild2Theme/>
    <br />
    <FunctionChild3Theme/>
    </>
  )
};

const FunctionChild1Theme = ()=>{
    const {themeStyles,toggleTheme, setToggleTheme} = useContext(ThemeContext);

    return (
        <>
    <div style={themeStyles}>FunctionChild1Theme
    <div><button onClick={()=>setToggleTheme(!toggleTheme)}>Toggle tHEME</button></div>
    </div>
    
    </>
    )
}
const FunctionChild2Theme = ()=>{
    const {themeStyles} = useContext(ThemeContext);

    return <div style={themeStyles}>FunctionChild2Theme</div>
}
const FunctionChild3Theme = ()=>{
    const {themeStyles} = useContext(ThemeContext);

    return <div style={themeStyles}>FunctionChild3Theme</div>
}

export default FcomponentWebDev;