import React, { Component } from 'react'
import { CounterContext } from './CounterContext'

export default class Ccomponent extends Component {
  render() {
    return (
        <><hr/>
      <div>Class Component</div>
      <div>{this.props.counter}</div>
      <CounterContext.Consumer>
       {(value)=>{
        return <div>{value}</div>;
       }}
      </CounterContext.Consumer>
      </>
    )
  }
}
