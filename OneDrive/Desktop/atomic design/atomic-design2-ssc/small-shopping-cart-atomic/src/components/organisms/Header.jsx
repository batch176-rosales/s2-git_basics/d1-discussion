import React from 'react';
import styled from 'styled-components';

export default function Header() {
  
    return (
    <HeaderWithStyled>
    <header>
        <HeaderMenuWithStyled>
    
        <div>
            < a href="/" className='styled-link'>TOP</a>
        </div>
        <div>
        <HeaderMenuLinksWithStyled>
            < a href="/" className='styled-link'>デザインをつくる</a>
            </HeaderMenuLinksWithStyled>
        </div>
        <div>
        <HeaderMenuLinksWithStyled>
            < a href="/" className='styled-link'>広告プランをつくる</a>
            </HeaderMenuLinksWithStyled>
        </div>
        <div>
        <HeaderMenuLinksWithStyled>
            < a href="/" className='styled-link'>効果検証レポート</a>
            </HeaderMenuLinksWithStyled>
        </div>
        <div>
        <HeaderMenuLinksWithStyled>
            < a href="/" className='styled-link'>Cart</a>
            </HeaderMenuLinksWithStyled>
        </div>
        
        </HeaderMenuWithStyled>

    </header>
    </HeaderWithStyled>
  );
};

const HeaderMenuWithStyled = styled.div `
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
    .styled-link{
    padding:50px;
    color: black;
    text-decoration:none;
    }
`;
// const HeaderWithStyled = styled.div `
//     .styled-link{
//     color: black;
//     text-decoration:none;
//     }
// `;


export const HeaderWithStyled= styled.div `
    // background-color:#e0e0e0;
    background-color: rgba(255, 255, 255, 1);
    box-shadow: 0px 0px 12px 3px rgba(0, 0, 0, .1);
    padding: 1.2rem;
    border-radius:1.6rem;
    box-sizing:border-box;
    margin:1rem;
`;
const HeaderMenuLinksWithStyled = styled.div`
    text-align: center;
    padding-left:22px;
    padding-right:22px;
    border-left: 1px solid #bdbdbd;
`;