import React,{useReducer} from 'react'


const initialState={
    firstCounter:0
};
const reducer = (state, action) =>{
    switch(action.type){
        case 'inc':
            return {firstCounter: state.firstCounter + action.value }
        case 'dec':
            return{firstCounter: state.firstCounter - action.value}
        case 'reset':
            return initialState
        
        default:
            return state
    }

}
const CounterTwoUseReducer = () => {
    const [count, dispatch] = useReducer(reducer, initialState);

  return (
    <>
        <div>CounterTwoUseReducer</div>
        <div>{count.firstCounter}</div><div>
          <button onClick={()=>dispatch({type:'inc', value:1})}>Increment</button>
          <button onClick={()=>dispatch({type:'dec', value:1})}>Decrement</button>
          <button onClick={()=>dispatch({type:'inc', value:5})}>Increment 5</button>
          <button onClick={()=>dispatch({type:'dec', value:5})}>Decrement 5</button>
          <button onClick={()=>dispatch({type:'reset'})}>Reset</button>
      </div>
      </>

  )
};

export default CounterTwoUseReducer;