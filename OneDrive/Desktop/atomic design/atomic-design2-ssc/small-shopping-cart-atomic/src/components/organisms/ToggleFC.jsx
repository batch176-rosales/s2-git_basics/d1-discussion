import React, { useState, useEffect } from "react";

const ToggleFC = () => {
    const [ time, setTime ] = useState(new Date().toString());
    const [ message, setMessage]= useState(false);
    const [message2, setMessage2] = useState("posts");
    const [ data, setData ] = useState([]);
  
    
    const showDate= () =>{
        setTime(new Date().toString());
    }
   
    
    useEffect(()=>{
        fetch(`https://jsonplaceholder.typicode.com/${message2}`)
 
            .then(response => response.json())
            .then(json => setData(json)
       )
    },[message2])
  
  return (
    <>
   
    <div>

          <div>{time}</div>
          <div><button onClick={showDate}>Show Date</button></div>
          <div>{message ? "Functional Component": "Changed Functional Component"}</div>
          <div><button onClick={()=>setMessage(!message)}>Change Message</button></div>

      </div>
      <hr /> <br />
      <div>
        <div>
            <button onClick={()=>setMessage2('posts')}>Posts</button>
            <button onClick={()=>setMessage2('users')}>Users</button>
            <button onClick={()=>setMessage2('comments')}>Comments</button>
        </div>
        <div> <h1>{message2}</h1></div>
      </div>
      
      <div>
        {data.map((item)=>(
            <pre key ={item.id}>{JSON.stringify(item)}</pre>
        ))}
      </div>

      </>
   );
};

export default ToggleFC;
