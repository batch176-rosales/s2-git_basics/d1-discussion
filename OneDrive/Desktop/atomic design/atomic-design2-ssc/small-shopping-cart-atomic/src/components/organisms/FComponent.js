import React, { useContext } from 'react';
import { CounterContext } from './CounterContext';


const FComponent= ()=> {
    const {counter, setCounter} = useContext(CounterContext);
  return (
    <><hr/>
    <div>Function Component</div>
    <div>{counter}</div>
    {/* <div>{value}</div> */}
    <div><button onClick={()=>setCounter(counter+1)}>Increment</button></div>
    <Fchild />
    </>
  )
};

const Fchild = () =>{
    const {counter, setCounter} = useContext(CounterContext);
    return(
        <><hr />
        <div>Function Child Component</div>
        <div>{counter}</div> 
        <div><button onClick={()=>setCounter(counter-1)}>Decrement</button></div>
        <br /></>

    )
}

export default FComponent;