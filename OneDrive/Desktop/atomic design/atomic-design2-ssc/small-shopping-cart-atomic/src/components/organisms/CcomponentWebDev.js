import React, { Component } from 'react'
import { ThemeContext } from './UseContextIndex';

export default class CcomponentWebDev extends Component {
  render() {
    // const {toggleTheme, themeStyles} = 
    return (
      <>
      {/* <div>Class Themee</div> */}
      <ThemeContext.Consumer>
        {(value)=>{
          return <div style ={value}>CLASS THEME</div>
        }}
      </ThemeContext.Consumer>
      <br />
      </>
    )
  }
}
