import React from 'react';
import styled from 'styled-components';
import { HeaderWithStyled } from './Header';


export default function CartItems() {
  return (
    <CartBlockWithStyled>
    <CartItemsWithStyled>
        <h2>CartItems</h2>
    </CartItemsWithStyled>
    </CartBlockWithStyled>
  )
}

const CartItemsWithStyled = styled.aside`
display: flex;
justify-content: space-between;
flex:col-1;

`;
const CartBlockWithStyled = styled(HeaderWithStyled)`

`;