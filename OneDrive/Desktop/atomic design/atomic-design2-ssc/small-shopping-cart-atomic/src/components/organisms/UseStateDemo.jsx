import React, { useState } from 'react';
import { useEffect } from 'react';

const UseStateDemo = () => {
    const [ name1, setName1 ] = useState("Hello, Depish");
    const [ name2, setName2 ] = useState(true);
    const [ cart, setCart ] = useState(true);
    const [ quantity, setQuantity ] = useState(1);
    const clamp = (min, max) => (v)=> v <= min ? min : v >= max ? max : v;
    const clampValue = clamp(1, 10);
    const [ name3, setName3 ] =useState("");
    const [ names, setNames ] = useState ([]);

    const addToCart = ()=>{
        setCart(!cart);
    };
    const decrement = () => {
        setQuantity(clampValue(quantity-1));
    };
    const increment = () => {
        setQuantity(clampValue(quantity+1));
    };
    const addName = (e) =>{
        e.preventDefault();
        setNames([...names,
            {id:names.length, 
            name3}])
        setName3("");
    };
    useEffect(()=>{
        console.log('component mount!');
    })
  return (
    <>
    <div>Static String using useState</div><br />
    <div>
        <div>
         {name1}
        </div>
        <button onClick={()=>setName1('Hello, Nikesh')}>Click Me</button>
    </div><hr/>
    <div> Dynamic button using useState (boolean) | changeName</div>
    <div>
        <div>Hello, {name2 ? "John": "Juan"}</div>
        <button onClick={()=> setName2(!name2)}>ClICK ME </button></div><hr/>
    <div> Dynamic button using useState (boolean) | ADD TO CART and Remove</div>
    <div>
        <button onClick={addToCart}>{cart ? "Add to Cart": "Remove"}</button>
    </div><hr/>
    <div> COUNTING NUMBER WITH DISABLE BUTTON FROM 1 AND 10</div>
    <div>
        <button onClick={decrement}>-</button>
        <button>{quantity}</button>
        <button onClick={increment}>+</button>
    </div><hr/>
    <div> form input (array)</div>
    <div>
        <form onSubmit={addName}>
            <input 
                type="text"
                value={name3}
                placeholder="add names"
                onChange={ (e) =>setName3(e.target.value)}
                />
                <button>Submit</button>
        </form>
        
    </div>
    <div>
            { names.map((item)=>(
                <pre key={item.id}>{item.name3}</pre>
           ) )}
            
        </div>
    </>

  )
};

export default UseStateDemo;

