import React, {useState, useEffect} from 'react';
import ToggleFC from './ToggleFC';


const UseEffectTutorial = () => {
  const [ toggleBtn, setToggleBtn ] = useState(true);
  const [ windowWidth, setWindowWidth ] = useState(window.innerWidth);
  const handleResize = ()=>{
    setWindowWidth(window.innerWidth);
}
useEffect(()=>{
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize)
    }
},[])

  return (
    <>
    <hr />
    <div>
      <button onClick={()=>setToggleBtn(!toggleBtn)}>Toggle Fuction Component</button>
      {toggleBtn ? <ToggleFC /> : ""}
    </div>
    
    <div>{windowWidth}</div>
    </>
  )
};

export default UseEffectTutorial;
