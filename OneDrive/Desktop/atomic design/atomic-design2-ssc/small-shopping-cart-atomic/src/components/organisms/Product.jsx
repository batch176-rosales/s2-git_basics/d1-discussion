import React from 'react';
import styled from 'styled-components';

export default function Product(props) {
    const {product} = props;
  return (
    <><ProductWithStyled>
          <img src={product.image} alt={product.name} className="small"></img>
      </ProductWithStyled>
      <h3>{product.name}</h3>
      <div>{product.price}</div>
      <ButtonWithStyled>Add To Cart</ButtonWithStyled>

      </>
    
  )
};

const ProductWithStyled = styled.div`
.small{
  max-height: 8rem;
}
    
`;
const ButtonWithStyled = styled.div `

`;