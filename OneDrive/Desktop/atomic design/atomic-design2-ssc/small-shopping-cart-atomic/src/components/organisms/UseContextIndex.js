import React, { useState } from 'react';
import Ccomponent from './Ccomponent';
import FComponent from './FComponent';
import {CounterContext} from './CounterContext';
import FcomponentWebDev from './FcomponentWebDev';
import CcomponentWebDev from './CcomponentWebDev';


export const ThemeContext = React.createContext();

const UseContextIndex = ()=> {
    const [counter, setCounter] = useState(0);
    const [toggleTheme, setToggleTheme]=useState(false);


    const themeStyles = {
        backgroundColor: toggleTheme ? 'gray' : "white",
        color: toggleTheme ? "white" : "black"
    }

  return (
    <><hr/> <br/>
    {/* --------Web Dev Simplified---------- */}
    <div>***********************useContext****************</div>
    
    <ThemeContext.Provider value ={{themeStyles,toggleTheme,setToggleTheme}}>
    <div>
        <button onClick={()=>setToggleTheme(!toggleTheme)}>Toggle Theme</button>
    </div>
    <FcomponentWebDev />
    {/* <CcomponentWebDev /> */}
    </ThemeContext.Provider>
   




    {/* ------------------ */}
    <hr/> <br/>
    <div>App Content</div>
    <div>{counter}</div>
    <div>
        <button onClick={()=>setCounter(counter+1)}>Increment</button>
        <button onClick={()=>setCounter(counter-1)}>Decrement</button>
    </div><hr/>
    <CounterContext.Provider value = {{counter, setCounter}}>
    <FComponent/>
    {/* <Ccomponent counter={counter} /> */}
    </CounterContext.Provider>
    </>
  )
};

export default  UseContextIndex;