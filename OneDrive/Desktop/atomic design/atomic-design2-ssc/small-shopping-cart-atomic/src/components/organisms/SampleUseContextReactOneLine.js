import React,{ useState, useContext } from 'react';
import SampleUseContextFcomponent from './SampleUseContextFcomponent';



export const CounterContext = React.createContext();

export default function SampleUseContextReactOneLine() {
    const [count, setCount] = useState(0);
    


  return (
    <><hr/><br />
    <div>****This is sample useContext using one line React****</div>
    <CounterContext.Provider value={{count, setCount}}>
    <div>APP COMPONENT</div>
    <div>{count}</div>
    <div>
        <button onClick={()=>setCount(count+1)}>Increment</button>
        <button onClick={()=>setCount(count-1)}>Decrement</button>
    </div>
    <hr/>
    <SampleUseContextFcomponent />
    
    </CounterContext.Provider>
    
    </>
  )
};


