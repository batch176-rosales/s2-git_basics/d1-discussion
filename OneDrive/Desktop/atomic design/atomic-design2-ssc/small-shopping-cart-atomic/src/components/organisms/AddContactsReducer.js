import React,{useState,useReducer} from 'react';

const initialState=[{
    id: Date.now(),
    name:"Debbie",
    email:"debbie@gmail.com"
}];

const reducer = (state, action)=>{
    switch(action.type){
        case 'add':
            return [...state,action.payload];
        case 'delete':
            return state.filter((contact) => {
                return contact.id !== action.payload.id;
            })
        default:
            throw new Error();
           
    }

}

const AddContactsReducer=()=> {
    const [name, setName]=useState("");
    const [email, setEmail] =useState("");
    const [contacts, dispatch]=useReducer(reducer,initialState);
    
    const addContact = (e)=>{
        e.preventDefault();
        const contact ={
            id:Date.now(),
            name,
            email
        };
        setName("");
        setEmail("");
        dispatch({type:'add', payload: contact});
        
    }


  return (
    <>
    <div>AddContactsReducer</div>
    <div>
          <form onSubmit={addContact}>
            <input
                type="text"
                value={name}
                placeholder="name"
                onChange={(e) => setName(e.target.value)} />
                  <br />
            <input
                type="text"
                value={email}
                placeholder="email"
                onChange={(e) => setEmail(e.target.value)} />
                <div>
                    <button>Add contact</button>
                </div>

          </form>
          </div>
          <div>
            <ul>
            {contacts.map((contact)=>{
                return (
                    <><li key={contact.id}>{contact.name}</li>
                    <li>{contact.email}</li>
                    <button onClick={()=>dispatch({type:'delete', payload:{id:contact.id}})}>Delete</button>
                </>
                )
            })}
            </ul>

      </div>
      </>
  )
};


export default AddContactsReducer;