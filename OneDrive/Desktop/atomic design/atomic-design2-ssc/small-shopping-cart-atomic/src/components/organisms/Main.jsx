import React from 'react';
import styled from 'styled-components';
import {HeaderWithStyled} from './Header';
import Product from './Product';

export default function Main(props) {
    const {products} = props;
  return (
    <MainWithStyled>
        <h2>Products</h2>
    <ProductsWithStyled>
        {products.map((product)=>(
            <Product key={product.id} product={product}></Product>
        ))}
    </ProductsWithStyled>
    </MainWithStyled>
    
  );
};

const MainWithStyled = styled(HeaderWithStyled)`
    flex:2;
    display: flex;
    justify-content: space-between;
`;

const ProductsWithStyled = styled.div`
 display: flex;
  justify-content: space-between;
`;