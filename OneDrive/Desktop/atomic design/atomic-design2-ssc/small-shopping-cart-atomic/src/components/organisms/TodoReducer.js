import React from 'react'

export default function TodoReducer({todo, dispatch}) {
  return (
    <div>
        <span style={{color: todo.complete ? '#AAA' : '#000'}}>{todo.name}</span>
        <button onClick={()=>dispatch({type:'toggleTodo', payload:{id:todo.id}})}>TOggle</button>
        <button onClick={()=>dispatch({type:'delete', payload:{id:todo.id}})}>Delete</button>
    </div>
  )
}
