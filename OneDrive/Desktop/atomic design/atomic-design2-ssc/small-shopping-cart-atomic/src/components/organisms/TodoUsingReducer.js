import React, { useState, useReducer } from 'react';
import TodoReducer from './TodoReducer';

const initialState = [];
const reducer = (todos, action) =>{
    // eslint-disable-next-line default-case
    switch (action.type) {
        case 'addTodo':
            return [...todos, action.payload]
        case 'toggleTodo':
            return todos.map (todo =>{
                if (todo.id === action.payload.id){
                    return {...todo, complete: !todo.complete}
                }
                return todo
            })
        case 'delete':
            return todos.filter((todo)=>{
                return todo.id !== action.payload.id;
            })
        default:
            return todos
    }
}

const TodoUsingReducer = () => {
    const [name, setName] = useState("");
    const [todos, dispatch]= useReducer (reducer, initialState);

const handleSubmit = (e) => {
    e.preventDefault();
    const newTodo = {
        id:Date.now(),
        name,
        complete:false
    }
    dispatch({type:"addTodo", payload:newTodo})
    setName("");

}
// console.log(todos);

  return (
    <>
    <div>TodoUsingReducer</div>
    <form onSubmit={handleSubmit}>
          <input 
            type ="text" value ={name} onChange={e=>setName(e.target.value)}/>
      </form>
      <div>
        {todos.map( todo =>{
            return <TodoReducer key={todo.id} todo= {todo} dispatch ={dispatch} />
        
        })}
      </div>
      </>
  )
};

export default TodoUsingReducer;