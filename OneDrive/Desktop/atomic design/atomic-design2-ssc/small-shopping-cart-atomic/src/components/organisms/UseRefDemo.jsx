import React, { useState, useRef, useEffect } from 'react';


const UseRefDemo = () => {
    const [name, setName]=useState("");
    const inputElement = useRef("");
    const [counter, setCounter] = useState(0);
    const prevValue = useRef(0);

    const resetName = ()=>{
        setName("");
        inputElement.current.focus();
    }
    useEffect (()=>{
        prevValue.current = counter;
        console.log('useref rendered');

    },[counter]);

  return (
    <><br /><hr /> 
    <div>
        <input
            type="text"
            ref={inputElement}
            value={name}
            onChange={(e)=>setName(e.target.value)}/>
             <button onClick={resetName}>Reset</button>
    </div>
    <div>My name is {name}</div>
    <hr />
    <div> Random Counter : {counter}</div>
    <div>Previous Value: {prevValue.current}</div>
    <div>
        <button onClick={()=> setCounter(Math.ceil(Math.random() * 100))}>Generate Number</button>
    </div>

    </>
  )
};

export default UseRefDemo;