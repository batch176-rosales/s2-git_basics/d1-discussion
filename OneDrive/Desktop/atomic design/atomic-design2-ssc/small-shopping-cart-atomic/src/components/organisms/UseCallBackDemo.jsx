import React, { useState, useEffect, useCallback, useMemo } from 'react';
import Lists from './Lists';

const UseCallBackDemo = () => {
  const [num, setNum] = useState(1);
  const [theme, setTheme] = useState(false);
  // const [numbers, setNumbers]=useState([]);

  const themeChanged = () => {
    setTheme(!theme);
  };
  const sequenceNum = useCallback(
    (incrementer) => {
      return [num + incrementer, num + 1 + incrementer, num + 2 + incrementer];
    },
    [num]
  );
  const themeStyless = {
    backgroundColor: theme ? 'red' : 'blue',
    color: theme ? 'white' : 'black',
  };
  // useEffect(()=>{
  //     setNumbers(sequenceNum())

  // },[sequenceNum]);
  return (
    <>
      <hr /> <br />
      <div>useCallback Demo</div>
      <div>
        <input
          type="number"
          value={num}
          onChange={(e) => setNum(parseInt(e.target.value))}
        />
      </div>
      <div style={themeStyless}>
        <div>
          <button onClick={themeChanged}>Toggle theme</button>
        </div>
        <Lists sequenceNum={sequenceNum} />
      </div>
    </>
  );
};

export default UseCallBackDemo;
