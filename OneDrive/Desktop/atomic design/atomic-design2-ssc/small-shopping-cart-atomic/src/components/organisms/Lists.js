import React, { useEffect, useState } from 'react'

export default function Lists({sequenceNum}) {
    const [numbers,setNumbers]=useState ([]);

    useEffect(()=>{
        setNumbers(sequenceNum(5))
        console.log('updating numbers');
    },[sequenceNum])
    

  return numbers.map((item,index)=><div key={index}>{item}</div>)
  
  
}
