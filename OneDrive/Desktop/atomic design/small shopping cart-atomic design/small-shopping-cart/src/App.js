import './App.css';
import Header from './components/organisms/Header';
import Main from './components/organisms/Main';
import Footer from './components/organisms/Footer';
function App() {
  return (
    <>
    <Header />
    <Main />
    {/* <Footer /> */}
    </>
  );
}

export default App;
