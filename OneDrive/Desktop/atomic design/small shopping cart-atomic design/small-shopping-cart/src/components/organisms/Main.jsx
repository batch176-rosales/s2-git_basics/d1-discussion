import React, {useState} from 'react';
import styled from 'styled-components';

const Main = () =>{
    const [name, setName] = useState("Debbie");
    const [defaultName, setDefaultName] = useState(true);
    const [cart, setCart] = useState(true);
    const [count, setCount] = useState(0);


    const changeName = () =>{
        // using string
        setName("Debbie Joy Rosales");
          console.log("you clicked me");

    }
    const changeNameAlternate = () =>{
        setDefaultName (!defaultName);
    }
    const changeCartBtn = ()=>{
        setCart(!cart);
    }
    const increment = () =>{
        setCount(count + 2);
    }
    const decrement = () =>{
        setCount(count - 1);
    }


  return (

    <MainWithStyled>
        <BlockWithStyled>
        <div>Hello, {name}!</div>
        <button onClick={changeName}>Click Me to change your name!</button>
        </BlockWithStyled>

        <BlockWithStyled>
            <div>{defaultName ? "Hello" : "Hello, Debbie"}</div>
            <button onClick={changeNameAlternate}>Click me</button>
        </BlockWithStyled>
        
        <BlockWithStyled>
            <button onClick={changeCartBtn}>{cart ? "Add to Cart" : "Remove"}</button>
        </BlockWithStyled>
        <hr />
        <CountWithStyled>
        <button onClick={decrement}>-</button>
        <div>{count}</div>
        <button onClick={increment}>+</button>
        </CountWithStyled>

    </MainWithStyled>
  )

};

const MainWithStyled = styled.div`
    font-size: 1.5em;
    text-align: center;
    color: orange;

`;
const BlockWithStyled = styled.div`
    padding-bottom:50px;

`;
 const CountWithStyled = styled.div`
 display:flex;
 justify-content: center;
 `;


export default Main;


