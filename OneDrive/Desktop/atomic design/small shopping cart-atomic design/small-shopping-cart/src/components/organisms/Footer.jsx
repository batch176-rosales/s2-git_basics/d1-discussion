import React from 'react'
import styled from 'styled-components';

export default function Footer() {

  return (
    <FooterWithStyled>&copy; FutureScope Corp.</FooterWithStyled>
  )
};

const FooterWithStyled = styled.div `
    display: flex;
    justify-content: center;
    align-items:center;
    color: #757575;
    font-size: 100%;
`;
