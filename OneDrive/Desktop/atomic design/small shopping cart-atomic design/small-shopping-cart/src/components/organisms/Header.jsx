import React from 'react';
import styled from 'styled-components';


export default function Header() {

  return (
    
    <header>
        <HeaderWithStyled>
      
        <div className='link-with-styled'>
            <a href="/">TOP</a>
        </div>
        <div className='link-with-styled'>
            <a href="/">Create Design</a>
        </div>
       <div className='link-with-styled'>
            <a href="/">Create Ad</a>
        </div>
       <div className='link-with-styled'>
            <a href="/">Reports</a>
        </div>
       <div className='link-with-styled'>
            <a href="/">Usage</a>
        </div>
    
        </HeaderWithStyled>
    </header>

    
  );
}
const HeaderWithStyled=styled.div`
display: flex;
    flex-direction : row;
    justify-content: center;
    align-items: center;
    color: black;
   
    .link-with-styled{
        text-decoration: none;
        color: black;
        padding:50px;
    }
   
`;

const HeaderMenuWithStyled = styled.div `
  


`;