import './App.css';
import { useState } from 'react';
//  import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Registration from './pages/Registration';
import Login from './pages/Login';
import ContactUs from './pages/Contact';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import { UserProvider } from './UserContext';
import { ProductProvider} from './context';
import AdminPage from './pages/AdminPage';
import Logout from './pages/Logout';
import Products from '../src/pages/Products';
import ProductScreen from '../src/screens/ProductScreen';

function App() {
  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') ==='true'
})

  const unsetUser = () =>{
    localStorage.clear();
  }

  return (
    <UserProvider value = {{ user, setUser, unsetUser}}>
    <ProductProvider>
  <BrowserRouter>
  {/* <AppNavbar /> */}
    
   <Routes>
            <Route>
            <Route path="/" element={<Home />} />
              <Route path="/register" element={<Registration />} />
              <Route path="/login" element={<Login />} />
              <Route path="/contact" element={<ContactUs />} />
              <Route path="/products" element={<Products />} />
              {/* <Route path="/" element={<HomeScreen />} /> */}
              <Route path="/product/:name" element={<ProductScreen />} />

              <Route path="/admin" element={<AdminPage />} />
              <Route path="/logout" element={<Logout />} />
              
            </Route>
          </Routes>
        </BrowserRouter>
        </ProductProvider>
    </UserProvider>
  );
}

export default App;
 