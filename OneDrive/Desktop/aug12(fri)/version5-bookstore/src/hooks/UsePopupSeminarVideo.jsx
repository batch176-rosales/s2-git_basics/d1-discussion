import React from 'react';
import styled from 'styled-components';
import ReactPlayer from 'react-player';
import { Icon } from '@blueprintjs/core';
// import Config from '../Configs/Environment';
import enclosure from '../components/products/enclosure.mp4';

export default function UsePopupSeminarVideo() {
  return (
    <>
    
    {/* <SkippableTextWithStyled>
                    <Icon icon={'cross'} iconSize={ 24 } />
                </SkippableTextWithStyled> */}

    
   
      
    <video src={enclosure} width="600" height="400" controls type="video/mp4">
      
     </video>
    
          
     
      </>
  )
};


const RobotaVideoWithStyled = styled.div`
    padding : 0 50px;
    position : relative;
    background-color : rgba(45, 45, 45, 0.6);
    border : none;
    max-width : 900px;
`;
const VideoGroupWithStyled = styled.div`
    // position : relative;
    // width : 800px;
    // height : 500px;
`;
const VideoWithStyled = styled.div`
    display: block;
    position: absolute;
`;
const SkipButtonWithStyled = styled.div`
    position : absolute;
    width: 50px;
    height: 50px;
    top : 5px;
    right : 5px;
    line-height : 50px;
    text-align : center;
    color: #ffffff;

    font-size : 120%;
    font-weight : 900;

    display : flex;
    justify-content : center;
    align-items : center;
`;

const SkippableTextWithStyled = styled(SkipButtonWithStyled)`
    cursor : pointer;
    font-size : 160%;
`;

const DiskippableTextWithStyled = styled(SkipButtonWithStyled)`
    cursor : not-allowed;
    line-height : 25px;
`;

const MutedTextWithStyled = styled.div`
    color: #efefef;
    line-height : 60px;
    height : 60px;
    text-align : center;
`;
