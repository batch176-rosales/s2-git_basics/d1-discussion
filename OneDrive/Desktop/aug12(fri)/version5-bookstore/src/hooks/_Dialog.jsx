import React, { useState } from 'react';
import { Dialog as BpDialog } from "@blueprintjs/core";

const _Dialog = ({
    children,
    open : defaultOpen,
    dlableClose,
    ...props
}) => {

    const [ open, setOpen ] = useState(defaultOpen);

    const handleClose = () => {
        ! dlableClose && setOpen(false);
    };

    const dialogProps = {
        isOpen : open,
        onClose : handleClose,
        ...props,
    };

    return (
        <BpDialog {...dialogProps}>
            { children }
        </BpDialog>
    )
}

export default _Dialog;