import { useState, useEffect } from 'react';

const useWindowResize = () => {
    const [width, setWidth] = useState(0);
    const [height, setHeight] = useState(0);

    useEffect(() => {
        handleUpdateWindowSize();
        window.addEventListener('resize', handleUpdateWindowSize);
        return () => window.removeEventListener('resize', handleUpdateWindowSize);
    }, []);

    const handleUpdateWindowSize = () => {
        const windowWidth = document.body.clientWidth;
        const windowHeight = document.body.clientHeight;
        setWidth(windowWidth);
        setHeight(windowHeight);
    };

    useEffect(() => {
        const intervalId = setInterval(handleUpdateWindowSize, 150);
        return () => {
            clearInterval(intervalId);
        }
    }, []);

    return [width, height];
};

export default useWindowResize;