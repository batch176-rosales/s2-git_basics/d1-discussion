import React from 'react';
import styled from 'styled-components';
import Dialog from './_Dialog';
import  useWindowResize from './useWindowResize';

const SubWindow = ({ children, ...props }) => {

    const [ width, height ] = useWindowResize();

    const dialogProps = {
        width,
        height,
        ...props,
    };

    return (
        <DialogWithStyled {...dialogProps}>
            { children }
        </DialogWithStyled>
    )
}

const DialogWithStyled = styled(Dialog)`
    max-width: 640px;
    max-height: ${({ height }) => height ? `${ height - 60 }px` : '100%'};
    width: 100%;
    height: 100%;
    border-radius : 22px;
    border: solid 1px #bdbdbd;
`;

export default SubWindow;