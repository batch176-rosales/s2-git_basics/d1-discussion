import {Divider, Drawer, Paper, Typography, useMediaQuery, Button} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { Box } from '@mui/system';
import { useUIContext } from '../../context/ui';
import { Colors } from '../../styles/theme';
import IncDec from "../ui/incdec";
import CartColumns from './CartColumns';
import CartList from './CartList';
import { ProductConsumer } from '../../context';
import Title from './Title';
import EmptyCart from './EmptyCart';
import context from '../../context';


export default function Cart(){
    const {cart, setShowCart, showCart } = useUIContext();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('md'));

    const cartContent = cart.map(item => (
        <Box key = {item.id} className="container-fluid">
         
             <Box display="flex" flexDirection={"row"}
                // display="flex"
                sx={{ pt:2, pb:2}}
                alignItems="center"
                 className="container-fluid"
                // margin={'auto'}
                 justifyContent={"space-between"}
                >
                   
                <img src={item.image} style={{width:'5rem', height:'5rem' }} className="img-fluid" alt={item.name}/>
                
                {/* <Box display="flex" flexDirection={"row"}> */}
                {!matches && 
                    <>
                    
                    <Typography variant="body" sx={{  mx:5, pl:2 }}>
                            ₱{item.price}
                        </Typography>
                        <Typography sx={{  mx:0,pr:6 }}><span className="btn btn-black">-</span>
                    <span className="btn btn-black mx-1">{item.count}</span>
                    <span className="btn btn-black">+</span></Typography>
                    <Typography>
                    <div className="cart-icon">
                    <i className="fa fa-trash"/>
                </div>
                        </Typography>
                        <Typography sx={{  mx:2,pl:5 }}><strong>₱ {item.price} </strong></Typography>
                        
                   
                        {/* <Typography variant="subtitle2">{item.description}
                        </Typography> */}
                        {/* <IncDec /> */}
                        </>
                        }
                
                
               
                </Box>
                
                {matches && 
                <>
                <Typography variant="h5">{item.name}</Typography><Typography variant="body1" sx={{ textDecoration: 'underline', mb: 2 }}>
                    ₱{item.price}
                </Typography><Typography variant="subtitle2">{item.description}</Typography>
                </>}
                <Divider variant='center' />
        </Box>
    ));

    return(
        <Drawer open={showCart} 
                onClose={() => setShowCart(false)}
                anchor="right"
                PaperProps={{
                    sx: {
                        width: matches ? '100%':730,
                        background:Colors.light_gray,
                        borderRadius:0
                    },
                }}>
                    {cart.length > 0 ? 
                     <Box 
                     sx={{ p: 2 }}
                     display="flex"
                     justifyContent={"center"}
                     flexDirection="column"
                     alignItems="center"
                     >
                         <Typography variant='h3' color={Colors.black}>
                             Your Cart
                         </Typography>
                         <Paper
                             elevation={0}
                             sx={{
                                 mt:2,
                                 width:'110%',
                                 padding:4,
     
                             }}>
                                  {/* {cartContent} */}
                                  <CartColumns />
                                  {cartContent} 
                             </Paper>
                             <Button sx={{mt:4}} variant="contained">
                                 Proceed to payment
                             </Button>
                
                 </Box>:(
                 <Box sx={{p:4,}} display="flex" justifyContent={"center"} flexDirection="column" alignItems="center">
                    <Typography variant={matches ? "h5":"h3"}color={Colors.black}>
                    Your cart is empty!
                    </Typography>
                  
                    
                 </Box>
                 )}
                <Button onClick={()=>setShowCart(false)}> Close </Button>
           
           
        </Drawer>

    );
}
