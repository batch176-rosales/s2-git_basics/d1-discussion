import {styled,Divider, Drawer, List, ListItemButton, ListItemText} from "@mui/material";
import { useUIContext } from "../../context/ui";
import CloseIcon from "@mui/icons-material/Close";
import { DrawerCloseButton } from "../../styles/appbar";
import { lighten } from "polished";
import { Colors } from "../../styles/theme";
import { useContext} from 'react';
import UserContext from '../../UserContext';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';




const MiddleDivider = styled((props) => (
  <Divider variant="middle" {...props} />
))``;

export default function AppDrawer() {
  const { drawerOpen, setDrawerOpen } = useUIContext();
  const { user } = useContext(UserContext);
  

  return (
    <>
      {drawerOpen && (
        <DrawerCloseButton onClick={() => setDrawerOpen(false)}>
          <CloseIcon
            sx={{
              fontSize: "2.5rem",
              color: lighten(0.09, Colors.secondary),
            }}
          />
        </DrawerCloseButton>
      )}
      <Drawer open={drawerOpen}>
        <List>
          <ListItemButton>
            <ListItemText> Home </ListItemText>
          </ListItemButton>
          <MiddleDivider />
          <MiddleDivider />
          <ListItemButton>
            <ListItemText>Products</ListItemText>
          </ListItemButton>
          <MiddleDivider />
          <ListItemButton>
            <ListItemText>About Us</ListItemText>
          </ListItemButton>
          <MiddleDivider />
          <ListItemButton>
            <ListItemText>Contact Us</ListItemText>
          </ListItemButton>
          <MiddleDivider />
          {(user.accessToken !== null) ?
          
          <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
          :
          <>
          <Nav.Link as={Link} to="/login">
            Log in
          </Nav.Link>
          </> 
      }
        </List>
      </Drawer>
    </>
  );
}