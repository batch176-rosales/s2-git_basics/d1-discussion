/* eslint-disable jsx-a11y/alt-text */
import { Box, Button,Dialog, DialogContent,DialogContentText,DialogActions,Container, Grid } from "@mui/material";
import { storeProducts } from "../../data";
import SingleProduct from "./SingleProduct";
import { useTheme } from "@mui/material/styles";
import { useMediaQuery } from "@mui/material";
import SingleProductDesktop from "./SingleProductDesktop";
import styled from 'styled-components';
// import thumbnail from './bgthumbnail.jpg';
import startBtn from './startBtn.png';
import { useState } from "react";
import UsePopupSeminarVideo from "../../hooks/UsePopupSeminarVideo";
import ReactPlayer from 'react-player';
import { Icon } from '@blueprintjs/core';
import WebSeminar from "./WebSeminar";
import dummyData from "../../dummyData";
// import Carousel from "react-elastic-carousel";
import ArrowLeftBtn from './ArrowLeft';
import ArrowRightBtn from './ArrowRight';
import ReactCardSlider from "./ReactCardSlider";

export default function Products(props) {
  const sliderClick = (slider)=>{
    alert("hello world");
  }

  const slides = [
    {image:"https://picsum.photos/200/300",clickEvent:sliderClick},
    {image:"https://picsum.photos/600/500",clickEvent:sliderClick},
    {image:"https://picsum.photos/700/600",clickEvent:sliderClick},
    {image:"https://picsum.photos/500/400",clickEvent:sliderClick},
    {image:"https://picsum.photos/200/300",clickEvent:sliderClick},
    {image:"https://picsum.photos/800/700",clickEvent:sliderClick},
    {image:"https://picsum.photos/300/400",clickEvent:sliderClick},
  ]



  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("md"));

  const {items} = dummyData;

  const renderProducts = storeProducts.map((product) => (
    <Grid item key={product.id} xs={2} sm={4} md={4} display="flex" flexDirection={'column'} alignItems="center">
      {matches ? (
        <SingleProduct product={product} matches={matches} />
      ) : (
        <SingleProductDesktop product={product} matches={matches} />
      )}
    </Grid>
  ));
  const handlePrevClick = () => {
    const slider = document.getElementById("slider");
        slider.scrollLeft = slider.scrollLeft + 500;
  }
  const handleNextClick = () => {
    const slider = document.getElementById("slider");
    slider.scrollLeft = slider.scrollLeft - 500;
}
  return (
    <Container>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        justifyContent="center"
        sx={{ margin: `20px 4px 10px 4px` }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {renderProducts}
      </Grid>
      <hr />
      <h1>Web Seminar</h1>

      <Block>

        <WebSeminar items={items} />
        <ArrowLeftBtn
          onClick={handlePrevClick}
          style={{
            position: 'absolute',
            top: '40px',
            left: -40,
            width: '40px',
            height: '40px',
            cursor: 'pointer',
            // display: page === 1 ? "none" : "block",
            display: 'block',
          }} />

        <ArrowRightBtn
          onClick={handleNextClick}
          style={{
            position: 'absolute',
            top: '40px',
            right: '30px',
            width: '40px',
            height: '40px',
            cursor: 'pointer',
            display: 'block',
          }} />

      </Block>
      <hr />

      <SliderBlockWithStyled>
        <ReactCardSlider slides={slides} />
      </SliderBlockWithStyled>
    </Container>
  );
};

const  Block = styled.div`
    position: relative;
    top:20px;
    left:32px;
    // overflow-x: hidden;
    display: flex;
    flex-direction: row;
    // padding-left: 45px;
`;

const  SliderBlockWithStyled = styled.div`
width: 100%;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
`;

//joy