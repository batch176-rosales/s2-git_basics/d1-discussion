import "../App.css";
import '../Contact.css';
import { Container, Typography, Box, Stack, Grid, Button } from "@mui/material";
import Appbar from "../components/appbar";
import { ThemeProvider } from "@mui/system";
import theme from "../styles/theme";
import { UIProvider } from "../context/ui";
import Footer from "../components/footer";
import AppDrawer from "../components/drawer";
import SearchBox from "../components/search";
import Cart from "../components/cart";
import Banner from "../components/banner";
import Products from "../components/products";
import "../index.css";
 import data from "../featuredData";
import { Link } from "react-router-dom";
import { useEffect, useReducer, useState } from "react";
import axios from 'axios';
import logger from 'use-reducer-logger';
import { Product } from "../styles/product";
import Promotions from "../components/promotions";
import { Row, Col } from "react-bootstrap";
import ProductWrapper from "../components/ProductWrapper";
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';



const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, products: action.payload, loading: false };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }   
};

 

export default function Prroducts () {
  const [{ loading, error, products }, dispatch] = useReducer(logger(reducer), {
    products: [],
    loading: true,
    error: '',
  });
  // const [products, setProducts] = useState([]);
  useEffect(()=>{
    const fetchData = async () => {
      // const result = await axios.get('/api/products');
      // setProducts(result.featuredData);

      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const result = await axios.get('/api/products');
        dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
      } catch (err) {
        dispatch({ type: 'FETCH_FAIL', payload: err.message });
      }

    };
    fetchData();
  }, []);
  
   return (
     <ThemeProvider theme={theme}>
     <Container
       disableGutters
       maxWidth="xl"
       sx={{
         background: "#DEF2F1",
       }}
     >
       <Stack>
         <UIProvider>
            <header>
           <Appbar />
           </header>
           <Promotions />
           
           {/* <Footer /> */}
           <SearchBox />
           <AppDrawer />
           <Cart />
         
     
     <main>
            <h1>Featured Products</h1>
            <div className="products">
            {loading ? (
              <LoadingBox />
            ) : error ? (
              <MessageBox variant="danger">
                {error}
              </MessageBox>
            ) : (
              <Row>
            {data.products.map((product) =>(
              <Col key ={product.name} sm={6} md={4} lg={3} className="mb-3">
                <ProductWrapper product={product}>
                </ProductWrapper>
                </Col>
            ))}
            </Row>
            )}
            </div>
           </main>
           <Box display="flex" justifyContent="center" sx={{ p: 4 }}>
              <Typography variant="h4">Our Products</Typography>
            </Box>
            <Products />
           <Footer />
           </UIProvider>
       </Stack>
       
     </Container>
   </ThemeProvider>
 );
}
